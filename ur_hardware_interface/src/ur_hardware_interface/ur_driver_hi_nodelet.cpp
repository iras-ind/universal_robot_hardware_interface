#include <ur_hardware_interface/ur_driver_hi_nodelet.h>
#include <pluginlib/class_list_macros.h>

PLUGINLIB_EXPORT_CLASS(itia::control::UrDriverHwIfaceNodelet, nodelet::Nodelet)

namespace itia
{
  namespace control
  {
    void UrDriverHwIfaceNodelet::onInit()
    {
      
      m_console_name = getPrivateNodeHandle().getNamespace()+" type: UrHwIfaceNodelet";
      
      ROS_INFO("[%s] STARTING", m_console_name.c_str());
      m_stop = false;
      
      std::vector<std::string> joint_names;
      if (!getPrivateNodeHandle().getParam("joint_names", joint_names))
      {
        ROS_FATAL_STREAM(getPrivateNodeHandle().getNamespace()+"/joint_names' does not exist");
        ROS_FATAL("ERROR DURING STARTING HARDWARE INTERFACE '%s'", getPrivateNodeHandle().getNamespace().c_str());
        return;
      }

      std::shared_ptr<ur_hw::HardwareInterface> ur_hw=std::make_shared<ur_hw::HardwareInterface>(joint_names);

//      double sampling_period = 1.0 / (static_cast<double>(ur_hw->m_ur_driver_hw->getControlFrequency()));
//      ROS_INFO("%s: Sampling period set equal to %f s",getPrivateNodeHandle().getNamespace().c_str(),sampling_period);
//      m_period=ros::Duration(sampling_period);
//      getPrivateNodeHandle().setParam("sampling_period", sampling_period);

      m_hw=ur_hw;

      m_main_thread = std::thread(&itia::control::UrDriverHwIfaceNodelet::mainThread, this);
      
    }
    
    
  }
}
